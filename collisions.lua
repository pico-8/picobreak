--function swept_aabb(b1,b2,normalx,normaly)
function swept_aabb(b1,b2)
    -- distance
    local xinventry local yinventry -- how far away the closest edges of the objects are from each other
    local xinvexit  local yinvexit -- distance to the far side of the object

    -- +1 : 1 pixel difference for left and bottom collision
    if b1.vx>0 then
        xinventry=b2.x-(b1.x+b1.w)
        xinvexit=(b2.x+b2.w)-b1.x
    else
        xinventry=(b2.x+b2.w)-b1.x+1
        xinvexit=b2.x-(b1.x+b1.w)+1
    end

    if b1.vy>0 then
        yinventry=b2.y-(b1.y+b1.h)
        yinvexit=(b2.y+b2.h)-b1.y
    else
        yinventry=(b2.y+b2.h)-b1.y+1
        yinvexit=b2.y-(b1.y+b1.h)+1
    end

    -- TO DEBUG
    -- delete this later
    xien=xinventry  yien=yinventry
    xiex=xinvexit   yiex=yinvexit

    -- time
    -- find time of collision and time of leaving for each axis (no divide per zero avoid)
    local xentry    local yentry
    local xexit     local yexit

    if b1.vx==0 then
        xentry=-32768
        xexit=32768
    else
        xentry=xinventry/b1.vx
        xexit=xinvexit/b1.vx end
    if b1.vy==0 then
        yentry=-32768
        yexit=32768
    else
        yentry=yinventry/b1.vy
        yexit=yinvexit/b1.vy end

    -- TO DEBUG
    -- delete this later
    xen=xentry  yen=yentry
    xex=xexit   yex=yexit

    -- find the earliest/latest times of collisionfloat
    local entrytime
    local exittime

    entrytime=max(xentry,yentry)
    exittime=min(xexit,yexit)

    -- if there was no collision
    if entrytime>exittime or (xentry<0 and yentry<0) or xentry>1 or yentry>1 then
        return 1
    else -- if there was a collision
        -- calculate normal of collided surface
        if xentry>yentry then
            if xinventry<0 then
                b2.normalx=1
                b2.normaly=0
            else
                b2.normalx=-1
                b2.normaly=0
            end
        else
            if yinventry<0 then
                b2.normalx=0
                b2.normaly=1
            else
                b2.normalx=0
                b2.normaly=-1
            end
        end
        -- return the time of collisionreturn entrytime
        return entrytime
    end
end
